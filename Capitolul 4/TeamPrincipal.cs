﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formula1
{
    public class TeamPrincipal
    {
        public int TeamPrincipalID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int Age { get; set; }

        // 1:1 relationship (Constructor - TeamPrincipal)
        public int Constructor { get; set; }
        public Constructor CurrentConstructor { get; set; }

        public override string ToString()
        {
            return "Prenume: " + this.FirstName + "\n" +
                "Nume: " + this.LastName + "\n" +
                "Age: " + this.Age + "\n"; 
        }

    }
}
