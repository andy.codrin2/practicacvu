﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Formula1
{
    class Formula1Context : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=Formula1DB;Trusted_Connection=True;");
        }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            // Relationships
            modelBuilder.Entity<Driver>()
            .HasOne<Constructor>(d => d.CurrentConstructor)
            .WithMany(c => c.Drivers)
            .HasForeignKey(d => d.Constructor);

            modelBuilder.Entity<GrandPrix>()
            .HasOne<Driver>(g => g.CurrentDriver)
            .WithMany(d => d.GrandPrixes)
            .HasForeignKey(g => g.Winner);

            modelBuilder.Entity<TeamPrincipal>()
            .HasOne<Constructor>(t => t.CurrentConstructor)
            .WithOne(c => c.TeamPrincipal)
            .HasForeignKey<TeamPrincipal>(t => t.Constructor);
            
            modelBuilder.Entity<Incident>()
            .HasOne<Driver>(i => i.CurrentDriver)
            .WithMany(d => d.Incidents)
            .HasForeignKey(i => i.DriverID);

            modelBuilder.Entity<Incident>()
            .HasOne<GrandPrix>(i => i.CurrentGrandPrix)
            .WithMany(d => d.Incidents)
            .HasForeignKey(i => i.GrandPrixID);

            // Dezactivare autoincrementare pentru DriversID
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<Driver>()
                .Property(d => d.DriverID)
                .ValueGeneratedOnAdd()
                .HasAnnotation("DatabaseGenerated", DatabaseGeneratedOption.None);

        }

        public DbSet<Constructor> Constructors { get; set; }
        public DbSet<Driver> Drivers { get; set; }
        public DbSet<TeamPrincipal> TeamPrincipals { get; set; }
        public DbSet<Incident> Incidents { get; set; }
        public DbSet<GrandPrix> GrandPrixes { get; set; }


    }
}
