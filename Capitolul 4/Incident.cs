﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formula1
{
    public class Incident
    {
        public int IncidentID { get; set; }

        public int DriverID { get; set; }
        public Driver CurrentDriver { get; set; }


        public int GrandPrixID { get; set; }
        public GrandPrix CurrentGrandPrix { get; set; }

    }
}
