﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;

namespace Formula1
{
    class Program
    {
        static void Main(string[] args)
        {
            var context = new Formula1Context();
            Console.WriteLine(".....................................................\n");

            bool rulare = true;
            while (rulare)
            {
                Console.WriteLine("Alegeti o operatie:\n" +
                    "1. Citire\n" +
                    "2. Stergere\n" +
                    "3. Adaugare\n" +
                    "4. Iesire");
                Console.Write("Operatia aleasa: ");
                string operatie = Console.ReadLine();
                Console.WriteLine();

                if (operatie == "1")
                {
                    bool optiune = true;
                    while (optiune)
                    {
                        Console.WriteLine("Ce tabel doriti a afisa?\n" +
                                                "1. Drivers\n" +
                                                "2. Constructors\n" +
                                                "3. TeamPrincipals\n" +
                                                "4. Revenire meniu principal");
                        Console.Write("Tabel ales: ");
                        Console.WriteLine();
                        string tabel = Console.ReadLine();

                        if (tabel == "1")
                        {
                            var answer = context.Drivers.ToList();
                            foreach (Driver d in answer)
                                Console.WriteLine(d);

                            optiune = false;
                        }

                        else if (tabel == "2")
                        {
                            var answer = context.Constructors.ToList();
                            foreach (Constructor d in answer)
                                Console.WriteLine(d);

                            optiune = false;
                        }

                        else if (tabel == "3")
                        {
                            var context1 = new Formula1Context();
                            var answer = context1.TeamPrincipals.ToList();
                            foreach (TeamPrincipal d in answer)
                                Console.WriteLine(d);

                            optiune = false;
                        }

                        else if (tabel == "4")
                        {
                            Console.WriteLine("Revenire la meniul principal...\n");
                            optiune = false;
                        }

                        else
                            Console.WriteLine("Nu ati ales o optiune corecta!\n");

                    }
                    
                }

                else if(operatie == "2")
                {
                    bool optiune = true;
                    while (optiune)
                    {
                        Console.WriteLine("Din ce tabel doriti a sterge:\n" +
                            "1. Drivers\n" +
                            "2. GrandPrix\n" +
                            "3. Revenire meniu principal");
                        Console.Write("Operatie: ");
                        string x = Console.ReadLine();
                        Console.WriteLine();

                        if (x == "1")
                        {
                            Console.Write("Introduceti numarul masinii soferului ce " +
                                "doriti a fi sters: ");
                            int nr = Convert.ToInt32(Console.ReadLine());
                            Driver driver = new Driver();
                            var answer = context.Drivers.ToList();
                            foreach (Driver d in answer)
                                if (d.CarNumber == nr)
                                    driver = d;
                            if (driver.DriverID == 0)
                                Console.WriteLine("Soferul cautat nu a fost gasit");
                            else
                                context.Remove<Driver>(driver);
                            context.SaveChanges();

                            optiune = false;
                        }

                        else if (x == "2")
                        {
                            Console.Write("Introduceti numele Marelui Premiu " +
                                "pe care doriti sa-l stergeti: ");
                            string nume = Console.ReadLine();
                            GrandPrix grandprix = new GrandPrix();
                            var answer = context.GrandPrixes.ToList();
                            foreach (GrandPrix d in answer)
                            {
                                if (d.Name == nume)
                                {
                                    grandprix = d;
                                    break;
                                }

                            }

                            if (grandprix.GrandPrixID == 0)
                                Console.WriteLine("Marele premiu cautat exista/" +
                                    "nu a avut loc in tara cautata in acest an!");
                            else
                                context.Remove<GrandPrix>(grandprix);
                            context.SaveChanges();
                            optiune = false;
                        }

                        else if (x == "3")
                        {
                            Console.WriteLine("Revenire la meniul principal...\n");
                            optiune = false;
                        }

                        else
                            Console.WriteLine("Nu ati ales o optiune corecta!\n");
                    }
                }

                else if(operatie == "3")
                {
                    bool optiune = true;
                    while (optiune)
                    {
                        Console.WriteLine("Din ce doriti a adauga:\n" +
                            "1. Driver\n" +
                            "2. GrandPrix\n" +
                            "3. Revenire meniu principal");
                        Console.Write("Operatie: ");
                        string op = Console.ReadLine();
                        Console.WriteLine();

                        if (op == "1")
                        {
                            int nr = 0;
                            int echipaID = 0;

                            bool alegere = true;
                            while (alegere)
                            {
                                bool ok = false;
                                Console.Write("Introduceti noul numar al masinii " +
                                "alese: ");
                                nr = Convert.ToInt32(Console.ReadLine());
                                var context3 = new Formula1Context();
                                var answer = context3.Drivers.ToList();
                                foreach (Driver d in answer)
                                {
                                    if (d.CarNumber == nr)
                                    {
                                        Console.WriteLine("Numarul introdus este deja ales!\n" +
                                            "Alegeti un alt numar!");
                                        ok = true;
                                        break;
                                    }

                                }

                                if (ok == false)
                                    alegere = false;
                            }

                            Console.Write("Nume: ");
                            string lastname = Console.ReadLine();
                            Console.Write("Prenume: ");
                            string firstname = Console.ReadLine();
                            Console.Write("Nationalitate: ");
                            string country = Console.ReadLine();

                            alegere = true;
                            while (alegere)
                            {
                                Console.Write("Introduceti numele echipei din care va " +
                                    "face parte: ");
                                string echipa = Console.ReadLine();
                                var context2 = new Formula1Context();
                                var answer = context2.Constructors.ToList();
                                bool ok = false;
                                foreach (Constructor d in answer)
                                {
                                    if (d.Name == echipa)
                                    {
                                        alegere = false;
                                        echipaID = d.ConstructorID;
                                        ok = true;
                                        break;
                                    }
                                }

                                if (ok == true)
                                    alegere = false;
                                else
                                    Console.WriteLine("Echipa introdusa nu exista!\n");
                            }


                            var Driver = new Driver()
                            {
                                LastName = lastname,
                                FirstName = firstname,
                                Constructor = echipaID,
                                CarNumber = nr

                            };

                            var context1 = new Formula1Context();
                            context1.Drivers.Add(Driver);
                            context1.SaveChanges();


                            optiune = false;
                        }

                        else if (op == "2")
                        {
                            string tara = "";
                            int driverIDr = 0;

                            var context5 = new Formula1Context();
                            var answer = context.GrandPrixes.ToList();
                            bool found = false;

                            Console.Write("Numele Marelui Premiu: ");
                            string nume = Console.ReadLine();
                            Console.WriteLine();

                            foreach (GrandPrix g in answer)
                            {
                                if (g.Name == nume)
                                {
                                    Console.WriteLine("Acest Mare Premiu este deja in baza de date!\n");
                                    found = true;
                                    break;
                                }
                            }

                            if (found == false)
                            {
                                bool ok = true;

                                Console.Write("Tara: ");
                                tara = Console.ReadLine();

                                while (ok)
                                {
                                    Console.Write("Numarul masinii castigatoare: ");
                                    int nr = Convert.ToInt32(Console.ReadLine());
                                    var context3 = new Formula1Context();
                                    var answer1 = context3.Drivers.ToList();
                                    foreach (Driver d in answer1)
                                    {
                                        if (d.CarNumber == nr)
                                        {
                                            driverIDr = d.DriverID;
                                            ok = false;
                                            break;
                                        }
                                    }

                                    if (ok == true)
                                        Console.WriteLine("Nu exista aceasta masina!");

                                }

                            }

                            var grandPrix = new GrandPrix()
                            {
                                Name = nume,
                                Country = tara,
                                Winner = driverIDr
                            };

                            context.GrandPrixes.Add(grandPrix);
                            context.SaveChanges();

                        }

                        else if (op == "3")
                        {
                            Console.WriteLine("Revenire la meniul principal...\n");
                            optiune = false;
                        }

                        else
                            Console.WriteLine("Alegeti o optiune prezentata mai sus!\n");

                    }
                }

                else if (operatie == "4")
                {
                    Console.WriteLine("Iesire meniu...\n");
                    rulare = false;
                }

                else
                    Console.WriteLine("Nu ati ales o optiune corecta!\n");

            }

        }
    }
    

    
}
