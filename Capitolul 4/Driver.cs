﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Formula1
{
    public class Driver
    {
        
        // Propreties
        public int DriverID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Nationality { get; set; }
        public int CarNumber { get; set; }

        // 1:M relationship (Constructor - Driver)
        public int? Constructor { get; set; }
        public Constructor CurrentConstructor { get; set; }

        // 1:M relationship (Driver - GrandPrix)
        public ICollection<GrandPrix> GrandPrixes { get; set; }

        // M:M relationship (Driver + GrandPrix = Incident)
        public IList<Incident> Incidents { get; set; }

        //Methods
        public override string ToString()
        {
            return "Prenume: " + this.FirstName + "\n"
                + "Nume: " + this.LastName + "\n"
                + "Nationalitate: " + this.Nationality + "\n"
                + "Numar masina: " + this.CarNumber + "\n";
        }



    }
}
