﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formula1
{
    public class GrandPrix
    {
        public int GrandPrixID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }

        // 1:M relationship
        public int? Winner { get; set; }
        public Driver CurrentDriver { get; set; }

        // M:M relationship (Driver + GrandPrix = Incident)
        public IList<Incident> Incidents { get; set; }
    }
}
