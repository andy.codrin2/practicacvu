﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Formula1
{
    public class Constructor
    {
        public int ConstructorID { get; set; }
        public string Name { get; set; }
        public string Country { get; set; }
        public float Budget { get; set; }

        // 1:M relationship (Constructor - Driver)
        public ICollection <Driver> Drivers { get; set; }

        // 1:1 relationship (Constructor - TeamPrincipal)
        public TeamPrincipal TeamPrincipal { get; set; }

        public override string ToString()
        {
            return "Nume: " + this.Name + "\n" +
                "Buget: " + this.Budget + "\n" +
                "Tara: " + this.Country +"\n";
        }
    }
}
