﻿using System;

namespace Problema_3
{
    class Program
    {
        static void Main(string[] args)
        {
            // Subpunctul a
            int n = 3, i, j;
            String[] nume = new string[n];
            for (i = 0; i < nume.Length; i++)
                nume[i] = Console.ReadLine();

            Console.WriteLine();

            // Subpunctul b
            for (i = 0; i < n; i++)
            {

                //Tranformare litere mari in mici
                nume[i] = nume[i].ToLower();

                //Punem un string intr-un vector de caractere
                char[] x = nume[i].ToCharArray();

                // Vectorul de aparitie
                int[] letter = new int[x.Length];
                for (j = 0; j < x.Length; j++)
                    letter[j] = 1;

                // Comparare caractere in vector
                for (j = 0; j < x.Length - 1; j++)
                {
                    if (letter[j] == 1)
                    {
                        for (int k = j + 1; k < x.Length - 1; k++)
                            if (x[j] == x[k])
                            {
                                letter[j]++;
                                letter[k] = 0; /* punem 0 pe pozitiile unde se afla litera
                                                    ne va folosi la afisare sa nu afisam de mai
                                                    multe ori aceeasi litera*/
                            }
                    }
                }

                // Afisare caracrere + aparitii 
                for (j = 0; j < x.Length; j++)
                {
                    if (letter[j] >= 1)
                    {
                        Console.Write(x[j]);
                        Console.Write(" ");
                        Console.WriteLine(letter[j]);
                    }
                }
                Console.WriteLine();
            }
        }
    }
}
