﻿using System;
using System.Collections.Generic;

namespace Capitolul_4
{
    class Program
    {
        static void Main(string[] args)
        {
            // Reduceri 
            Reducere promotie_martie = new Reducere("Promotie luna martie", new DateTime(2020, 3, 1));
            Reducere reducere_haine = new Reducere("Reducere pentru haine", new DateTime(2020, 8, 13));
            Reducere promotie_electrocasnice = new Reducere("Promotie electrocasnice", new DateTime(2021, 5, 14));
            Reducere reducere_jumate = new Reducere("Reducere cu cel putin 50%", new DateTime(2021, 12, 1));
            Reducere promotie_sarbatori_iarna = new Reducere("Promotie sarbatori de iarna", new DateTime(2021, 11, 1));
            Reducere reducere_casnice = new Reducere("Reducere lucruri casnice", new DateTime(2021, 5, 15));
            Reducere reducere_cincime = new Reducere("Reducere 20%", new DateTime(2021, 6, 23));
            Reducere promotie_craciun = new Reducere("Promotie craciun", new DateTime(2021, 12, 10));

            // Producatori
            Producator eMag = new Producator("eMAG");
            Producator IKEA = new Producator("IKEA");
            Producator ZARA = new Producator("ZARA");
            Producator Kaufland = new Producator("Kaufland");

            // Lista producatori
            List<Producator> producatori = new List<Producator>();
            producatori.Add(eMag);
            producatori.Add(IKEA);
            producatori.Add(ZARA);
            producatori.Add(Kaufland);

            // Reduceri pentru producatori
            eMag.adaugare_reducere(promotie_electrocasnice);
            eMag.adaugare_reducere(promotie_craciun);
            eMag.adaugare_reducere(reducere_jumate);

            IKEA.adaugare_reducere(reducere_casnice);
            IKEA.adaugare_reducere(reducere_cincime);
            IKEA.adaugare_reducere(promotie_martie);
            IKEA.adaugare_reducere(promotie_sarbatori_iarna);

            ZARA.adaugare_reducere(promotie_craciun);
            ZARA.adaugare_reducere(reducere_cincime);
            ZARA.adaugare_reducere(reducere_jumate);

            Kaufland.adaugare_reducere(reducere_haine);
            Kaufland.adaugare_reducere(reducere_cincime);
            Kaufland.adaugare_reducere(promotie_electrocasnice);
            Kaufland.adaugare_reducere(promotie_martie);

            // Lista clienti
            List<Client> clienti = new List<Client>();
            Client andy = new Client("andy.codrin2@gmail.com", Moneda.EUR);
            Client beatrice = new Client("mirtisbetty@gmail.com", Moneda.LEU);
            Client mark = new Client("mark.grigore@gmail.com", Moneda.LEU);
            Client julio = new Client("julio_ernest@gmail.com", Moneda.USD);
            Client adina = new Client("adina@gmail.com", Moneda.EUR);
            clienti.Add(andy);
            clienti.Add(beatrice);
            clienti.Add(mark);
            clienti.Add(julio);
            clienti.Add(adina);

            // Cataloage
            Catalog catalog_IKEA = new Catalog(new DateTime(2021, 1, 1), new DateTime(2021, 6, 30), IKEA.get_Lista_reduceri());
            Catalog catalog_Kaufland = new Catalog(new DateTime(2021, 1, 1), null, Kaufland.get_Lista_reduceri());
            Catalog catalog_ZARA = new Catalog(null, new DateTime(2021, 6, 30), ZARA.get_Lista_reduceri());
            Catalog catalog_eMAG = new Catalog(new DateTime(2021, 10, 24), new DateTime(2021, 12, 31), eMag.get_Lista_reduceri());

            // Produse
            Produs dulap_pax = new Produs("Dulap pax", new Pret(Moneda.LEU, 3000), 3, IKEA);
            Produs chiuveta = new Produs("Chiuveta", new Pret(Moneda.LEU, 800), 8, IKEA);
            Produs scaun_birou = new Produs("Scaun birou", new Pret(Moneda.LEU, 1000), 15, IKEA);
            Produs aragaz = new Produs("Aragaz", new Pret(Moneda.LEU, 450), 14, IKEA);

            Produs hanorac = new Produs("Hanorac", new Pret(Moneda.EUR, 18), 20, ZARA);
            Produs camasa = new Produs("Camasa", new Pret(Moneda.EUR, 32), 7, ZARA);
            Produs blugi = new Produs("Blugi", new Pret(Moneda.EUR, 20), 10, ZARA);
            Produs tricou = new Produs("Tricou", new Pret(Moneda.EUR, 33), 10, ZARA);

            Produs laptop = new Produs("Laptop", new Pret(Moneda.USD, 1100), 6, eMag);
            Produs telefon = new Produs("OnePlus 6", new Pret(Moneda.USD, 500), 12, eMag);
            Produs casti = new Produs("Casti Bluetooth", new Pret(Moneda.USD, 80), 8, eMag);
            Produs cablu_usbC = new Produs("Cablu USB", new Pret(Moneda.USD, 4), 30, eMag);

            Produs monopoly = new Produs("Joc Monopoly", new Pret(Moneda.LEU, 120), 10, Kaufland);
            Produs ciocolata_milka = new Produs("Ciocolata Milka", new Pret(Moneda.LEU, 25), 15, Kaufland);
            Produs fanta = new Produs("Fanta", new Pret(Moneda.LEU, 7), 60, Kaufland);
            Produs detergent_Ariel = new Produs("Detergent Ariel", new Pret(Moneda.LEU, 25), 30, Kaufland);

            catalog_eMAG.Add(laptop); catalog_eMAG.Add(telefon); catalog_eMAG.Add(casti); catalog_eMAG.Add(cablu_usbC);
            catalog_IKEA.Add(dulap_pax); catalog_IKEA.Add(chiuveta); catalog_IKEA.Add(scaun_birou); catalog_IKEA.Add(aragaz);
            catalog_Kaufland.Add(monopoly); catalog_Kaufland.Add(ciocolata_milka); catalog_Kaufland.Add(fanta); catalog_Kaufland.Add(detergent_Ariel);
            catalog_ZARA.Add(hanorac); catalog_ZARA.Add(camasa); catalog_ZARA.Add(blugi); catalog_ZARA.Add(tricou);

            // Adaugare produse favorite
            andy.adaugare_produsFavorit(laptop.getGuid());
            andy.adaugare_produsFavorit(monopoly.getGuid());
            andy.adaugare_produsFavorit(fanta.getGuid());

            beatrice.adaugare_produsFavorit(hanorac.getGuid());
            beatrice.adaugare_produsFavorit(cablu_usbC.getGuid());
            beatrice.adaugare_produsFavorit(dulap_pax.getGuid());
            beatrice.adaugare_produsFavorit(scaun_birou.getGuid());
            beatrice.adaugare_produsFavorit(casti.getGuid());

            mark.adaugare_produsFavorit(laptop.getGuid());
            mark.adaugare_produsFavorit(camasa.getGuid());
            mark.adaugare_produsFavorit(ciocolata_milka.getGuid());
            mark.adaugare_produsFavorit(aragaz.getGuid());

            julio.adaugare_produsFavorit(scaun_birou.getGuid());
            julio.adaugare_produsFavorit(dulap_pax.getGuid());
            julio.adaugare_produsFavorit(fanta.getGuid());

            adina.adaugare_produsFavorit(detergent_Ariel.getGuid());
            adina.adaugare_produsFavorit(telefon.getGuid());
            adina.adaugare_produsFavorit(casti.getGuid());
            adina.adaugare_produsFavorit(tricou.getGuid());

            /*
            Console.WriteLine(laptop.Pret.Valoare);
            laptop.Pret.SchimbarePret += Pret_SchimbarePret;
            laptop.Pret.Valoare = 300;
            Console.WriteLine("ABC: ");
            Console.WriteLine(laptop.Pret.Valoare +"\n");
            */
            List<Produs> a = new List<Produs>();
            a.Add(laptop);

            genDel<Produs> a1 = new genDel<Produs>(reducere_jumate.aplica);
            a1(a[0]);

            Console.WriteLine("Verificare aici: " + laptop.Pret.Valoare);


        }

    }

    public delegate void genDel<T>(T val);

    class Producator
    {
        protected string nume;
        protected List<Reducere> reduceri;

        public Producator(string nume)
        {
            this.nume = nume;
            this.reduceri = new List<Reducere>();
        }

        public void adaugare_reducere(Reducere r)
        {
            this.reduceri.Add(r);
        }

        public void afisare()
        {
            Console.WriteLine("Nume producator: " + this.nume);
            Console.WriteLine("Reduceri: ");
            for (int i = 0; i < reduceri.Count; i++)
            {
                Console.WriteLine("Nume reducere: " + reduceri[i].getNume());
                Console.WriteLine("Data: " + reduceri[i].getDataTime());
            }
        }

        public string getNume()
        {
            return this.nume;
        }

        public List<Reducere> get_Lista_reduceri()
        {
            return this.reduceri;
        }

    }

    class Reducere
    {
        protected string nume;
        protected DateTime data_time;

        public Reducere(string nume, DateTime data)
        {
            this.nume = nume;
            this.data_time = data;
        }

        public string getNume()
        {
            return this.nume;
        }

        public DateTime getDataTime()
        {
            return this.data_time;
        }

        public void aplica (Produs p)
        {
            Console.WriteLine("Pret initial: " + p.Pret.Valoare);
            p.Pret.SchimbareValoare += Pret_SchimbareValoare;
            p.Pret.Valoare = p.Pret.Valoare * 0.5m;
            Console.WriteLine("Pret final: " + p.Pret.Valoare);
        }

        private void Pret_SchimbareValoare(object sender, SchimbareValoareEventArgs e)
        {
            Console.WriteLine("Pret vechi: " + e.Pret_vechi);
            Console.WriteLine("Pret nou: " + e.Pret_nou);
        }
    }

    class Produs
    {
        protected Guid id;
        protected string nume;
        protected Pret pret;
        protected int stoc;
        protected Producator producator;

        public int Stoc
        {
            get
            {
                return stoc;
            }

            set
            {
                try
                {
                    if (value > 0)
                    {
                        if (this.stoc == value)
                            return;
                        
                        decimal stoc_vechi = this.stoc;
                        this.stoc = value;
                        OnSchimbareStoc(new SchimbareStocEventArgs(stoc_vechi, this.stoc));
                    }

                    else
                        throw new ArgumentOutOfRangeException("Stocul trebuie sa fie un numar intreg mai mare decat 0!");
                }
                catch (ArgumentOutOfRangeException e)
                {
                    Console.WriteLine(e);
                }

            }
        }

        public Produs(string nume, Pret pret, int stoc, Producator producator)
        {
            this.Stoc = stoc;
            this.id = Guid.NewGuid();
            this.nume = nume;
            this.producator = producator;
            this.Pret = pret;
        }

        public event EventHandler<SchimbareStocEventArgs> SchimbareStoc;

        protected virtual void OnSchimbareStoc(SchimbareStocEventArgs e)
        {
            SchimbareStoc?.Invoke(this, e);
        }

        public void afisare()
        {
            Console.WriteLine("Id: " + this.id + "\nNume: " + this.nume + "\nStoc: " + this.stoc
                + "\nProducator: " + this.producator.getNume());
        }

        public Guid getGuid()
        {
            return this.id;
        }

        public Pret Pret
        {
            get
            {
                return this.pret;
            }

            set
            {
                pret = value;
            }
        }

    }

    public class SchimbareValoareEventArgs : EventArgs
    {
        public readonly decimal Pret_vechi;
        public readonly decimal Pret_nou;

        public SchimbareValoareEventArgs(decimal pret_vechi, decimal pret_nou)
        {
            Pret_nou = pret_nou;
            Pret_vechi = pret_vechi;
        }
    }

    public class SchimbareStocEventArgs
    {
        public readonly decimal Stoc_vechi;
        public readonly decimal Stoc_nou;

        public SchimbareStocEventArgs(decimal stoc_vechi, decimal stoc_nou)
        {
            Stoc_nou = stoc_nou;
            Stoc_vechi = stoc_vechi;
        }
    }

    class Pret
    {
        protected decimal valoare;
        protected Moneda moneda;
        static Dictionary<Moneda, decimal> curs;

        public event EventHandler<SchimbareValoareEventArgs> SchimbareValoare;

        protected virtual void OnSchimbareValoare(SchimbareValoareEventArgs e)
        {
            SchimbareValoare?.Invoke(this, e);
        }

        public Pret(Moneda m, decimal val)
        {
            curs = new Dictionary<Moneda, decimal>();
            this.moneda = m;
            this.valoare = val;

            if(m.ToString() == "LEU")
            {
                curs.Add(Moneda.EUR, 0.2031m);
                curs.Add(Moneda.USD, 0.2390m);
            }
            
            else if(m.ToString() == "USD")
            {
                curs.Add(Moneda.EUR, 0.8497m);
                curs.Add(Moneda.LEU, 4.1843m);
            }

            else if (m.ToString() == "EUR")
            {
                curs.Add(Moneda.USD, 1.1769m);
                curs.Add(Moneda.LEU, 4.9247m);
            }
            
        }

        public decimal Valoare
        {
            get
            {
                return this.valoare;
            }

            set
            {
                // Verificare pret anterior
                if (this.valoare == value)
                    return;

                decimal pret_vechi = this.valoare;
                this.valoare = value;
                OnSchimbareValoare(new SchimbareValoareEventArgs(pret_vechi, this.valoare));
            }
        }

        public decimal ValoareCurs(Moneda m)
        {
            if(this.moneda.ToString() == m.ToString())
            {
                Console.WriteLine("Pretul este deja afisat in moneda ceruta: " + this.valoare + " " + this.moneda);
                return this.valoare;
            }

            else
            {
                curs.TryGetValue(m, out decimal rezultat);
                return this.valoare * rezultat;
            }
        }

    }

    class Client
    {
        protected string email;
        protected Moneda moneda;
        protected List<Guid> produseFavorite;
        protected string[] inbox;

        public Client(string email, Moneda m)
        {
            this.email = email;
            this.moneda = m;
            this.inbox = new string[10];
            this.produseFavorite = new List<Guid>();
        }

        public bool notifica(string mesaj)
        {
            try
            {
                for(int i=0; i < 10; i++)
                {
                    if(inbox[i] == null)
                    {
                        if (mesaj.Length > 60)
                        {
                            Console.WriteLine("Mesajul este prea lung!");
                            return false;
                        }
                            

                        else
                        {
                            inbox[i] = mesaj;
                            Console.WriteLine("Mesajul a fost adaugat!" + i);
                            return true;
                        }
                            
                    }
                }

                throw new OutOfMemoryException("Inbox-ul clientului are deja 10 mesaje!");

            }

            catch(OutOfMemoryException e)
            {
                Console.WriteLine(e);
                return false;
            }
        }

        public void adaugare_produsFavorit(Guid id)
        {
            for(int i = 0; i < this.produseFavorite.Count; i++)
            {
                if (produseFavorite[i] == id)
                {
                    Console.WriteLine("Produsul selectat se afla deja in lista de produse favorite a clientului!");
                    return;
                }
            }

            this.produseFavorite.Add(id);
        }

        public string getEmail()
        {
            return this.email;
        }

    }

    class Catalog: List<Produs>
    {
        protected List<Reducere> reduceri;
        DateTime? perioadaStart;
        DateTime? perioadaStop;
        protected List<Client> abonati;

        public Catalog(DateTime? start, DateTime? final, List<Reducere> r)
        {
            this.perioadaStart = start;
            this.perioadaStop = final;
            this.reduceri = new List<Reducere>();
            this.reduceri = r;
            this.abonati = new List<Client>();
        }

        public void afisare()
        {
            Console.WriteLine(this.perioadaStart + " " + this.perioadaStop);
        }

        public void abonare(Client c)
        {
            this.abonati.Add(c);
        }

    }

    public enum Moneda { EUR, USD, LEU}

    
}
