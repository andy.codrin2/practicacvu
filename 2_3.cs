﻿using System;
using System.Threading;

namespace Capitolul_3
{
    class Program
    {
        static void Main(string[] args)
        {
            Espressor.Pot p1 = new Espressor.Pot();
            Espressor.Water_Sensor s1 = new Espressor.Water_Sensor();
            Espressor espressor = new Espressor(s1, p1);
            espressor.start_espressor();

            bool rulare = true;
            bool optiune_corecta = false;
            string optiune;
            while (rulare)
            {
                Console.WriteLine("Alegeti ce doriti sa faceti\n" +
                    "<1> Adaugare apa in espressor\n<2> Incalzire apa\n" + 
                    "<3> Preparare & turnare cafea\n<4> Verificare nivel apa\n" +
                    "<5> Verificare praf cafea\n<6> Adaugare praf\n<7> Oprire espressor");
                Console.Write("Optiune: ");
                optiune = Console.ReadLine();
                optiune_corecta = false;

                
                while(optiune_corecta == false)
                {
                    // Adaugare apa in espressor
                    if (optiune == "1")
                    {
                        espressor.water_Sensor.adaugareApa();
                        optiune_corecta = true;
                    }

                    // Incalzire apa
                    else if (optiune == "2")
                    {
                        espressor.incalzire_apa();
                        optiune_corecta = true;
                    }

                    // Preparare cafea
                    else if (optiune == "3")
                    {
                        espressor.turnare_cafea_in_cana();
                        optiune_corecta = true;
                    }

                    // Verificare nivel apa
                    else if (optiune == "4")
                    {
                        Console.WriteLine("Verificare nivel apa...");
                        Thread.Sleep(500);

                        Console.WriteLine("In boiler se afla " + espressor.water_Sensor.getLevel() +
                            " ml de apa \n"); ;
                        optiune_corecta = true;
                    }

                    // Verificare daca este pus praf pentru cafea
                    else if (optiune == "5")
                    {
                        Console.WriteLine("Verificare filtru...");
                        Thread.Sleep(500);

                        if (espressor.filter == true)
                            Console.WriteLine("Filtrul are deja praf pentru cafea\n");
                        else
                            Console.WriteLine("Filtrul nu are praf pentru cafea\n");

                        optiune_corecta = true;
                    }

                    // Adaugare praf de cafea
                    else if (optiune == "6")
                    {
                        Console.WriteLine("Verificare praf...");
                        Thread.Sleep(500);

                        if (espressor.filter == true)
                            Console.WriteLine("Nu mai este nevoie sa adaugati praf pentru cafea, este deja pus!\n");

                        else
                        {
                            Console.WriteLine("Adaugare praf...");
                            Thread.Sleep(200);
                            espressor.filter = true;
                            Console.WriteLine("Praful a fost adaugat cu succes\n");
                        }

                        optiune_corecta = true;
                    }

                    // Oprire espressor
                    else if (optiune == "7")
                    {
                        Console.WriteLine("Se opreste espressorul...");
                        Thread.Sleep(1000);
                        optiune_corecta = true;
                        rulare = false;
                    }

                    else
                    {
                        Console.WriteLine("Nu ati ales o optiune corecta!" + "\n");
                        Console.Write("Optiune: ");
                        optiune = Console.ReadLine();
                    }
                        
                }
                
            }

        }
    }

    public class Espressor
    {
        public bool start_button;
        protected bool light_indicator;
        public bool boiler_heater;
        public bool filter;

        public class Water_Sensor
        {
            protected double level;
            protected int temperature;

            public Water_Sensor()
            {
                this.temperature = 20;
                this.level = 0;
            }

            public int getTemperature()
            {
                return this.temperature;
            }

            public double getLevel()
            {
                return this.level;
            }

            public bool adaugareApa()
            {
                if(this.level >= 1000)
                {
                    Console.WriteLine("In espressor se afla cantitatea maxima de apa, nu este nevoie " +
                        "sa introduceti apa!\n");
                    return true;
                }

                else
                {
                    Console.Write("Introduceti de la 200 ml pana la " + (1000 - this.getLevel())
                        + " ml de apa: ");
                    double cantitate = Convert.ToInt32(Console.ReadLine());

                    if (cantitate <= 0 || cantitate > (1000 - this.getLevel()))
                    {
                        Console.WriteLine("Ati introdus o valoare gresita!");
                        return false;
                    }

                    else
                    {
                        int noua_temeperatura = (int)((cantitate * 20 + this.level * this.temperature) / (cantitate + this.level));
                        this.level += cantitate;
                        this.setTemperature(noua_temeperatura);
                        Console.WriteLine("Ati introdus " + cantitate + " ml de apa.\nNoul nivel de apa " +
                            "este: " + this.level + " ml" + "\n");
                        return true;
                    }

                }
            }

            public void setTemperature(int temp)
            {
                this.temperature = temp;
            }

            public void scoatereApa(double d)
            {
                this.level = d;
            }
        }

        public Water_Sensor water_Sensor;

        public class Pot
        {
            protected bool pressure_sensor;

            public Pot()
            {
                this.pressure_sensor = false;
            }

            public bool check_prezenta()
            {
                return this.pressure_sensor;
            }

            public void set_true()
            {
                this.pressure_sensor = true;
            }
        }

        public Pot pot;

        public Espressor(Water_Sensor s1, Pot p1)
        {
            this.start_button = false;
            this.light_indicator = false;
            this.filter = false;
            this.water_Sensor = s1;
            this.pot = p1;
        }

        public void start_espressor()
        {
            if(this.start_button == false)
            {
                this.start_button = true;
                this.light_indicator = true;
                Console.WriteLine("Espressorul a pornit!\nNivel apa: " + this.water_Sensor.getLevel()
                    + " ml\n");
                this.boiler_heater = false;
            }
        }

        public bool getLight_indicator()
        {
            return this.light_indicator;
        }

        public void incalzire_apa()
        {
            Console.WriteLine("Verificare nivel apa...");
            Thread.Sleep(500);

            if(this.water_Sensor.getLevel() < 200)
            {
                Console.WriteLine("In boiler nu este suficienta apa pentru a porni incalzirea! (200 ml) \n" +
                    "Adaugati apa si apoi reveniti pentru a o incalzi. \n");
                return;
            }

            if(this.water_Sensor.getTemperature() >= 70)
            {
                Console.WriteLine("Apa are deja temperatura ideala pentru a face cafea! \n");
                return;
            }

            else
            {
                if(this.boiler_heater == false)
                {
                    Console.WriteLine("Asteptam cateva secunde sa se aprinda boilerul...");
                    this.boiler_heater = true;
                    Thread.Sleep(2000);
                    Console.WriteLine("Boilerul s-a aprins, urmeaza sa fie incalzita apa!");
                }

                Console.WriteLine("Asteptam sa se incalzeasca apa....");
                Thread.Sleep(4000);
                this.water_Sensor.setTemperature(100);
                Console.WriteLine("Apa a fost incalzita la temperatura de 100 de grade.\n");
                this.boiler_heater = false;
            }
        }

        public void turnare_cafea_in_cana()
        {
            bool mai_departe = true;
            string x;

            Console.WriteLine("Verificare nivel apa...");
            Thread.Sleep(400);
            if(this.water_Sensor.getLevel() < 200)
            {
                double nivel = 200 - this.water_Sensor.getLevel();
                Console.WriteLine("Nivelul de apa din boiler este prea mic pentru a preparea cafea. \n" +
                    "Adaugati cel putin " + nivel + "ml de apa pentru a putea continua operatiunea!\n" +
                    "Alegeti o optiune:\n<1> Adaugare apa\n<2> Intoarcere la meniul principal\n");
                

                while (mai_departe)
                {
                    Console.Write("Optiunea aleasa: ");
                    x = Console.ReadLine();

                    if (x == "1")
                    {
                        this.water_Sensor.adaugareApa();
                        mai_departe = false;
                    }

                    else if(x == "2")
                    {
                        mai_departe = false;
                        Console.WriteLine();
                        return;
                    }

                    else
                        Console.WriteLine("Apasati tasta 1 sau 2!");
                    
                }
            }

            Console.WriteLine("Verificare prezenta cana...");
            Thread.Sleep(500);
            if (this.pot.check_prezenta())
                Console.WriteLine("Cana este asezata in locul potrivit.");

            else
            {
                
                Console.WriteLine("Pe recipient nu se afla nicio cana! \n<1> Asezare cana pe recipient \n" +
                    "<2> Revenire la meniul principal");
                mai_departe = true;
                
                while (mai_departe)
                {
                    Console.Write("Optiunea aleasa: ");
                    x = Console.ReadLine();
                    if(x == "1")
                    {
                        Console.WriteLine("Asezare cana pe recipient...");
                        Thread.Sleep(1000);
                        this.pot.set_true();
                        Console.WriteLine("Cana a fost asezata pe recipient \n");
                        mai_departe = false;
                    }

                    else if(x == "2")
                    {
                        Console.WriteLine("Revenire la meniul principal...\n");
                        mai_departe = false;
                        Thread.Sleep(500);
                        return;
                    }

                    else
                    {
                        Console.WriteLine("Apsati tasta 1 sau 2!");
                    }
                }

            }

            Console.WriteLine("Verificare temperatura apei...");
            Thread.Sleep(500);

            if (this.water_Sensor.getTemperature() > 75)
            {
                Console.WriteLine("Apa are temperatura ideala pentru a prepara cafeaua. \n");
            }

            else
            {
                mai_departe = true;
                Console.WriteLine("Temperatura apei este mica. \nIncalzire apa...");
                Thread.Sleep(500);
                this.incalzire_apa();
            }

            Console.WriteLine("Verificare praf cafea...");
            Thread.Sleep(500);

            if (this.filter == true)
                Console.WriteLine("Exista destul praf pentru prepararea unei cafele.");

            else
            {
                Console.WriteLine("In filtru nu este destul praf pentru cafea. \n" +
                    "<1> Adugare praf in filtru \n<2> Revenire la meniul principal");
                mai_departe = true;

                while (mai_departe)
                {
                    Console.Write("Optiunea aleasa: ");
                    x = Console.ReadLine();

                    if(x == "1")
                    {
                        mai_departe = false;
                        this.filter = true;
                        Console.WriteLine("Adaugare praf...\n");
                        Thread.Sleep(2000);
                    }

                    else if(x == "2")
                    {
                        mai_departe = false;
                        Console.WriteLine("Revenire la meniul principal...\n");
                        Thread.Sleep(200);
                        return;

                    }

                    else
                        Console.WriteLine("Apsati tasta 1 sau 2!\n");
                   
                }

            }

            Console.WriteLine("Alegeti un cantitatea de cafea ce doriti sa fie turnata:\n" +
                "<1> 50ml \n<2> 75ml \n<3> 100ml \n<4> 200ml");
            mai_departe = true;

            while (mai_departe)
            {
                Console.Write("Optiunea aleasa: ");
                x = Console.ReadLine();

                if (x == "1")
                {
                    this.water_Sensor.scoatereApa(this.water_Sensor.getLevel() - 50);
                    this.water_Sensor.setTemperature(this.water_Sensor.getTemperature() - 5);
                    Console.WriteLine("Turnare cafea...");
                    Thread.Sleep(1000);
                    mai_departe = false;
                    Console.WriteLine("Operatiunea s-a terminat cu succes! \n" +
                        "Revenire la meniul principal...\n");
                    this.filter = false;
                    return;
                }

                else if (x == "2")
                {
                    this.water_Sensor.scoatereApa(this.water_Sensor.getLevel() - 75);
                    this.water_Sensor.setTemperature(this.water_Sensor.getTemperature() - 7);
                    Console.WriteLine("Turnare cafea...");
                    Thread.Sleep(1500);
                    mai_departe = false;
                    Console.WriteLine("Operatiunea s-a terminat cu succes! \n" +
                        "Revenire la meniul principal...\n");
                    Thread.Sleep(200);
                    this.filter = false;
                    return;
                }

                else if (x == "3")
                {
                    Console.WriteLine(this.water_Sensor.getLevel() - 100);
                    this.water_Sensor.scoatereApa(this.water_Sensor.getLevel() - 100);
                    this.water_Sensor.setTemperature(this.water_Sensor.getTemperature() - 8);
                    Console.WriteLine("Turnare cafea...");
                    Thread.Sleep(2000);
                    mai_departe = false;
                    Console.WriteLine("Operatiunea s-a terminat cu succes! \n" +
                        "Revenire la meniul principal...\n");
                    Thread.Sleep(200);
                    this.filter = false;
                    return;
                }

                else if (x == "4")
                {
                    this.water_Sensor.scoatereApa(this.water_Sensor.getLevel() - 200);
                    this.water_Sensor.setTemperature(this.water_Sensor.getTemperature() - 10);
                    Console.WriteLine("Turnare cafea...");
                    Thread.Sleep(4000);
                    mai_departe = false;
                    Console.WriteLine("Operatiunea s-a terminat cu succes! \n" +
                        "Revenire la meniul principal...\n");
                    Thread.Sleep(200);
                    this.filter = false;
                    return;
                }

                else if (x == "5")
                {
                    Console.WriteLine("Revenire la meniul principal...\n");
                    Thread.Sleep(200);
                    mai_departe = false;
                    return;
                }

                else
                    Console.WriteLine("Apasati tasta 1, 2, 3, 4 sau 5!");
            }

        }


    }

    
}
