-- Exercitiul 1 --
-- Afisati din ce tari provin concurentii si cati concurenti are fiecare tara --

SELECT Nationality, COUNT(ID) as 'Drivers'
FROM Drivers
Group BY Nationality

-- Exercitiul 2 --
-- Afisati toti concurentii care au victorii -- 
SELECT DISTINCT Drivers.Firstname, Drivers.Lastname
FROM Drivers
INNER JOIN GrandPrix ON Drivers.ID = GrandPrix.Winner




