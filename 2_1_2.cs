﻿using System;

namespace Problema_2
{
    class Program
    {
        static void Main(string[] args)
        {
            // Subpunctul a
            int i;
            string[] tokens = Console.ReadLine().Split();
            float [] a = new float[tokens.Length];

            for(i = 0; i < tokens.Length; i++)
                a[i] = Single.Parse(tokens[i]);

            // Subpunctul b

            for (i = 0; i < tokens.Length; i++)
            {
                Console.Write(a[i]);
                Console.Write(" ");
            }

            Console.WriteLine();
            // Subpunctul c
            float min = a[0];
            for (i = 1; i < tokens.Length; i++)
            {
                if (a[i] < min)
                    min = a[i];
            }
            Console.Write("Minimul este: ");
            Console.WriteLine(min);
                
        }
    }
}
