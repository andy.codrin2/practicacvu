﻿using System;
using System.Collections.Generic;

namespace Capitolul_2._2
{
    class Program
    {
        static void Main(string[] args)
        {
            //Subpunctul a
            Animal.Dimensiune d1 = new Animal.Dimensiune(2m, 3m, 4m);
            Animal.Dimensiune d2 = new Animal.Dimensiune(1m, 1m, 1m);
            Animal.Dimensiune d3 = new Animal.Dimensiune(3m, 2m, 2m);

            Carnivor lup = new Carnivor("Mircea", 75m, 55m, d1);
            Erbivor oaie = new Erbivor("Ana", 150m, 20m, d2);
            Omnivor urs = new Omnivor("Martin", 380m, 56m, d3);

            // Subpunctul b
            Carne sunca = new Carne(3m, 0.02m);
            Planta salata = new Planta(1.78m, 0.04m);

            // Subpunctul c
            lup.Mananca(sunca);
            lup.Mananca(sunca);

            oaie.Mananca(salata);
            oaie.Mananca(salata);
            oaie.Mananca(salata);

            urs.Mananca(sunca);
            urs.Mananca(salata);
            urs.Mananca(salata);
            urs.Mananca(salata);

            // Subpunctul d
            lup.Alearga(200m);
            oaie.Alearga(200m);
            urs.Alearga(200m);

            lup.Mananca(salata);
            oaie.Mananca(sunca);

            Console.WriteLine(CreeazaAnimal(TipAnimal.Pisica, "Pixi", 2m, 30m, d1));
            Console.WriteLine();

            // Subpunctul i
            List<Animal> lista_animale = new List<Animal>();
            Random random = new Random();
            Animal.Dimensiune dimensiune_random = new Animal.Dimensiune();

            for (int i = 0; i < 10; i++)
            {
                decimal greutate = new decimal(random.NextDouble() * 25);
                decimal viteza = new decimal(random.NextDouble() * 10);
                int index = random.Next(5);
                Console.WriteLine((TipAnimal)index);
                TipAnimal tip = (TipAnimal)index;
                dimensiune_random.setInaltime(new decimal(random.NextDouble() * 20));
                dimensiune_random.setLatime(new decimal(random.NextDouble() * 20));
                dimensiune_random.setLungime(new decimal(random.NextDouble() * 20));
                lista_animale.Add(CreeazaAnimal(tip, "Animal", greutate, viteza, dimensiune_random));
            }

            for (int i = 0; i < 10; i++)
            {
                Console.WriteLine(lista_animale[i] + "\n");
                if (lista_animale[i].GetType().Name == "Carnivor")
                    lista_animale[i].Mananca(sunca);
                else if (lista_animale[i].GetType().Name == "Erbivor")
                    lista_animale[i].Mananca(salata);
                else
                    lista_animale[i].Mananca(salata);
            }

            // Subpunctul j
            int nr_omnivore = 0;
            int nr_carnivore = 0;
            int nr_erbivore = 0;
            for (int i = 0; i < 10; i++)
            {
                if (lista_animale[i].GetType().Name == "Carnivor")
                    nr_carnivore++;
                else if (lista_animale[i].GetType().Name == "Erbivor")
                    nr_erbivore++;
                else if (lista_animale[i].GetType().Name == "Omnivor")
                    nr_omnivore++;
            }

            Console.WriteLine(nr_carnivore + " animale mananca carne");
            Console.WriteLine(nr_omnivore + " animale mananca mancare");
            Console.WriteLine(nr_erbivore + " animale mananca plante");


        }

        // Subpunctul h
        public static Animal CreeazaAnimal(TipAnimal tip, string nume, decimal greutate, decimal viteza, Animal.Dimensiune d1)
        {
            if(tip.ToString() == "Lup")
            {
                Carnivor carnivor = new Carnivor(nume, greutate, viteza, d1);
                return carnivor;
            }
            else if (tip.ToString() == "Urs" || tip.ToString() == "Pisica")
            {
                Omnivor omnivor = new Omnivor(nume, greutate, viteza, d1);
                return omnivor;
            }
            else 
            {
                Erbivor erbivor = new Erbivor(nume, greutate, viteza, d1);
                return erbivor;
            }

        }

    }

    public class Mancare
    {
        public decimal greutate;
        public decimal energie;

        public decimal Energie
        {
            get
            {
                return energie;
            }
            set
            {
                try
                {
                    if (value > 0 && value < 0.05m)
                        energie = value;
                    else
                        throw new ArgumentOutOfRangeException("Energia trebuie sa aiba o valoare in intervalul [0, 0.05]");
                }
                catch(ArgumentOutOfRangeException e) 
                {
                    Console.WriteLine("Energia trebuie sa aiba o valoare in intervalul [0, 0.05]");
                }
                
            }
        }
        
        public Mancare(decimal greutate, decimal energie)
        {
            this.Energie = energie;
            this.greutate = greutate;
        }
    }

    public class Carne : Mancare
    {

        public Carne(decimal greutate, decimal energie) : base(greutate, energie)
        {
        }

    }

    public class Planta : Mancare
    {
        public Planta(decimal greutate, decimal energie) : base(greutate, energie)
        {
        }
    }

    public abstract class Animal
    {
        public string nume;
        protected decimal greutate;

        public struct Dimensiune
        {
            decimal lungime;
            decimal latime;
            decimal inaltime;

            public Dimensiune(Dimensiune d1)
            {
                this.lungime = d1.lungime;
                this.latime = d1.latime;
                this.inaltime = d1.inaltime;
            }

            public Dimensiune(decimal lungime, decimal latime, decimal inaltime)
            {
                this.lungime = lungime;
                this.latime = latime;
                this.inaltime = inaltime;
            }

            public decimal getInaltime()
            {
                return this.inaltime;
            }

            public decimal getLatime()
            {
                return this.latime;
            }

            public decimal getLungime()
            {
                return this.lungime;
            }

            public void setInaltime(decimal s)
            {
                this.inaltime = s;
            }

            public void setLatime(decimal s)
            {
                this.latime = s;
            }

            public void setLungime(decimal s)
            {
                this.lungime = s;
            }

        }

        protected Dimensiune dimensiune;
        protected decimal viteza;
        protected Mancare[] stomac;
        protected int contor = 0; // cate alimente a mancat animalul respectiv
        static public int nr_animale;  // Subpunctul e

        public Animal(string nume, decimal greutate, decimal viteza, Dimensiune d1) 
        {
            this.nume = nume;
            this.greutate = greutate;
            this.viteza = viteza;
            this.dimensiune = d1;
            stomac = new Mancare[5];
        }

        public void Mananca(Mancare m)
        {
            // Conditie introdusa la subpunctul f
            if (this.GetType().Name.ToString() == "Carnivor" && m.GetType().Name.ToString() == "Carne"
                || this.GetType().Name.ToString() == "Erbivor" && m.GetType().Name.ToString() == "Planta"
                || this.GetType().Name.ToString() == "Omnivor")
            {
                if (m.greutate <= this.greutate / 8)
                {
                    Console.WriteLine("Mananca");
                    stomac[contor++] = m;
                }
                else
                {
                    Console.WriteLine("Mancarea introdusa are peste 1/8 din greutatea animalului");
                }
            }

            else
            {
                Console.WriteLine("Tipul acesta de animal mancanca alt tip de mancare!");
            }


        }

        public abstract double Energie();

        public void Alearga(decimal distanta)
        {
            decimal timp;
            decimal energie = (decimal)this.Energie();
            timp = distanta / (viteza / energie);
            Console.WriteLine("Animalul " + this.nume + " parcurge " + distanta + "m in " + timp + " secunde.");

        }

    }

    public class Carnivor : Animal
    {
        public Carnivor(string nume, decimal greutate, decimal viteza, Dimensiune d1) : base(nume, greutate, viteza, d1)
        {

        }

        public override double Energie()
        {
            decimal v1 = 0.2m, v2 = 1 / 5;
            decimal medie_greutate, sum = 0;

            for (int i = 0; i < base.contor; i++)
                sum = sum + base.stomac[i].energie;
            medie_greutate = sum / base.contor;
            double final = (double)(v1 - v2 * medie_greutate + sum);
            return final;
        }

        public override string ToString()
        {
            return "Tip animal: " + this.GetType().Name + "\n" + "Nume: " + this.nume + "\n" + "Greutate: " +
                this.greutate + "\n" + "Viteza: " + this.viteza + "\n"
                + "Dimensiuni: " + this.dimensiune.getLungime() + " " + this.dimensiune.getLatime() + 
                " " + this.dimensiune.getInaltime();
        }
    }

    public class Erbivor : Animal
    {
        public Erbivor(string nume, decimal greutate, decimal viteza, Dimensiune d1) : base(nume, greutate, viteza, d1)
        {

        }

        public override double Energie()
        {
            decimal v1 = 0.5m, v2 = 1 / 3;
            decimal medie_greutate, sum = 0;

            for (int i = 0; i < base.contor; i++)
                sum = sum + base.stomac[i].energie;
            medie_greutate = sum / base.contor;
            double final = (double)(v1 + v2 * medie_greutate + sum);
            return final;
        }

        public override string ToString()
        {
            return "Tip animal: " + this.GetType().Name + "\n" + "Nume: " + this.nume + "\n" + "Greutate: " +
                this.greutate + "\n" + "Viteza: " + this.viteza + "\n"
                + "Dimensiuni: " + this.dimensiune.getLungime() + " " + this.dimensiune.getLatime() +
                " " + this.dimensiune.getInaltime();
        }
    }

    public class Omnivor : Animal
    {
        public Omnivor(string nume, decimal greutate, decimal viteza, Dimensiune d1) : base(nume, greutate, viteza, d1)
        {

        }

        public override double Energie()
        {
            decimal v1 = 0.35m;
            decimal medie_greutate = 0, sum = 0;

            for (int i = 0; i < base.contor; i++)
            {
                sum = sum + base.stomac[i].energie;
                if (base.stomac[i].GetType().Name == "Carne")
                    medie_greutate = medie_greutate + -1.2m * base.stomac[i].greutate;
                else
                    medie_greutate = medie_greutate + 1.2m * base.stomac[i].greutate;
            }
                
            
            double final = (double)(v1 + medie_greutate + sum);
            return final;
        }

        public override string ToString()
        {
            return "Tip animal: " + this.GetType().Name + "\n" + "Nume: " + this.nume + "\n" + "Greutate: " +
                            this.greutate + "\n" + "Viteza: " + this.viteza + "\n"
                            + "Dimensiuni: " + this.dimensiune.getLungime() + " " + this.dimensiune.getLatime() +
                            " " + this.dimensiune.getInaltime();
        }
    }

    public enum TipAnimal { Lup, Urs, Oaie, Veverita, Pisica, Vaca }

}
