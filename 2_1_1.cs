﻿using System;

namespace Capitolul_1
{
    class Program
    {
        static void Main(string[] args)
        {
            // Subpunctul a
            int numar = 0;
            Boolean ok = false;
            while(ok == false)
            {
                Console.WriteLine("Introduceti un numar care are minim 3 cifre:");
                numar = Convert.ToInt32(Console.ReadLine());
                if (numar / 100 < 1 && numar / 100 > -1)
                    Console.WriteLine("Acesta nu este un numar de cel putin 3 cifre!");
                else
                    ok = true;
            }

            //Subpunctul b
            numar = Reverse(numar);

            //Subpunctul c
            Verific(numar);
            
        }

        static void Verific(int numar)
        {
            Boolean ok = false;
            if(numar >= 0)
            {
                for (int i = 1; i * i <= numar; i++)
                {
                    if (i * i == numar)
                        ok = true;
                }
            }
            
            if (ok == true)
                Console.WriteLine("Numarul introdus mai devreme in oglinda este patrat perfect");
            else
                Console.WriteLine("Numarul introdus mai devreme in oglinda NU este patrat perfect");
        }

        static int Reverse(int numar)
        {
            int x = 0, y;
            while(numar > 0)
            {
                y = numar % 10;
                x = x * 10 + y;
                numar = numar / 10;
            }

            return x;
        }

    }
}
